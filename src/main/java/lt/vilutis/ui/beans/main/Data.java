package lt.vilutis.ui.beans.main;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.bean.*;
import javax.faces.context.FacesContext;
import javax.validation.*;
import javax.validation.constraints.*;

import org.hibernate.validator.constraints.*;

/**
 * Backing bean for "main" flow
 * 
 * @author Zilvinas Vilutis
 */
@ManagedBean( name = "data", eager = true )
@ViewScoped
public class Data implements Serializable {

    private static final long serialVersionUID = -6701176578785764204L;

    @Min( 0 )
    @Digits( integer = 19, fraction = 0  )
    private Integer num;

    @DecimalMin( "0.0" )
    @Digits( integer = 19, fraction = 2  )
    private BigDecimal dec;

    @Pattern( regexp = "\\.*" )
    private String optional;

    @NotNull
    @NotEmpty
    private String required;

    @Email
    private String email;

    private final String disabled = "disabled";

    private Boolean minMaxRequired = false;
    
    private Integer min, max;
    
    @AssertTrue( message = "Minimum must be less than Maximum" )
    protected boolean getMinMax() {
        if ( min != null && max != null ) {
            return min <= max;
        }
        return true;
    }
    
    public Integer getNum() {
        return num;
    }

    public void setNum( Integer num ) {
        this.num = num;
    }

    public BigDecimal getDec() {
        return dec;
    }

    public void setDec( BigDecimal dec ) {
        this.dec = dec;
    }

    public String getOptional() {
        return optional;
    }

    public void setOptional( String optional ) {
        this.optional = optional;
    }

    public String getRequired() {
        return required;
    }

    public void setRequired( String required ) {
        this.required = required;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail( String email ) {
        this.email = email;
    }

    public String getDisabled() {
        return disabled;
    }

    public Integer getMin() {
        return min;
    }

    public void setMin( Integer min ) {
        this.min = min;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax( Integer max ) {
        this.max = max;
    }
    
    public Boolean getMinMaxRequired() {
        return minMaxRequired;
    }
    
    public void setMinMaxRequired( Boolean minMaxRequired ) {
        this.minMaxRequired = minMaxRequired;
    }
    
    @AssertTrue( message = "Minimum and Maximum must be entered!" )
    public boolean validateMinMaxRequired() {
        if ( Boolean.TRUE.equals( minMaxRequired ) ) {
            return min != null && max != null;
        }
        return true;
    }
    
    public String validate() {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<Data>> result = validator.validate( this );
        if ( result.isEmpty() ) {
            return "success";
        } else {
            for ( ConstraintViolation<Data> constraintViolation : result ) {
                FacesMessage message = new FacesMessage( constraintViolation.getMessage() );
                message.setSeverity( FacesMessage.SEVERITY_ERROR );
                FacesContext.getCurrentInstance().addMessage( "main:" +
                    constraintViolation.getPropertyPath().toString(),
                    message
                ); 
            }
            return "error";
        }
    }

}
