# validator — ( JSR 303 ) beans validation POC using JSF &amp; RichFaces

I'm using this project as a Proof Of Concept of JSR 303 beans validation samples for creating simple and complex UI &amp; backend rules using `JSR 303` on `JSF` + `RichFaces`.

This project also integrates with:

+   JSF 2.1
+   RichFaces 4.3.0
+   Hibernate-Validator 4.3.0

Source code is at `git@bitbucket.org:cikasfm/validator.git` or you can also browse it using a browser at [https://bitbucket.org/cikasfm/validator/src](https://bitbucket.org/cikasfm/validator/src)

The web app uses an embedded tomcat server, so all you need to do run the following commands:

    git clone git@bitbucket.org:cikasfm/validator.git
    # run tomcat using maven
    mvn tomcat:run
    # or package war using maven and run it executable jar
    mvn package
    java -jar target/validator-1.0-SNAPSHOT-war-exec.jar

**Note:** By default `Tomcat` will listen port `8080`. You can also add the optional `--httpPort` parameter to change the http connection port if `8080` is not available, e.g. `java -jar target/validator-1.0-SNAPSHOT-war-exec.jar --httpPort 8888`

( once ready to work ) the project shall be deployed on heroku at [http://validator.vilutis.lt/validator/](http://validator.vilutis.lt/validator/) ( or [http://zvalidator.herokuapp.com/validator/](http://zvalidator.herokuapp.com/validator/) ), to deploy to `heroku` you need to push to `git@heroku.com:zvalidator.git`.

Please feel free to fork this code and send any pull requests with updates and / or recommendations.

[Zilvinas Vilutis](https://code.vilutis.lt)